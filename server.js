const express = require("express");
const cors = require('cors');
const app = express();
require("dotenv").config();
require("./config/database").connect();

app.use(cors());
app.use(express.json());

const streamingHandler = require("./streamingHandler");

const UserEvent = require("./models/userLiveEvent");
const Stream = require("./models/stream");

app.get("/lives/:id", async (req,res) => {
    let lives = await Stream.find({_id: req.params.id,is_live: true});
    res.status(200).send(lives);
});

app.get("/lives", async (req,res) => {
    let lives = await Stream.find({is_live: true});
    res.status(200).send(lives);
});

app.get("/vods/:id", async (req,res) => {
    let vods = await Stream.find({_id: req.params.id, is_live: false});
    res.status(200).send(vods);
});

app.get("/vods", async (req,res) => {
    let vods = await Stream.find({is_live: false});
    res.status(200).send(vods);
});

app.get("/streams/:id", async (req,res) => {
    console.log("received request for stream with id: " + req.params.id);
    let stream = await Stream.find({_id: req.params.id});
    res.status(200).send(stream);
})

app.post("/", async (req, res) => {
    let title = req.body.title;
    let game_id = req.body.game_id || "";
    let user_id = JSON.parse(req.headers["user"]).user_id;
    let userEvent = await UserEvent.findOne({ user_id: user_id });
    console.log(userEvent)
    if (!userEvent) {
        console.log("stuff")

        await UserEvent.create({
            name: user_id,
            user_id: user_id
        })
        
        userEvent = {
            name: user_id,
            user_id: user_id
        }
    }
    let assetName = await streamingHandler.createLiveEvent(user_id);
    let ingestUrl = await streamingHandler.startLiveEvent(userEvent.name);
    let endpoint = await streamingHandler.createStreamingEndpoint(assetName, "output");
    let stream = await Stream.create({
        title: title,
        user_id: user_id,
        endpoint: endpoint,
        game_id: game_id,
        is_live: true
    })
    res.status(200).send({
        ingestUrl: ingestUrl,
        endpoint: endpoint,
        stream: stream
    });
})

app.delete("/", async (req, res) => {
    let user_id = JSON.parse(req.headers["user"]).user_id;
    await Stream.findOneAndUpdate({user_id:user_id, is_live:true},{is_live:false});
    await streamingHandler.stopStream(user_id, `liveOutput-${user_id}`);
    res.send("check the console!!");
});

app.get("/", async (req, res) => {
    let streams = await Stream.find();
    res.status(200).send(streams);
})

port = process.env.PORT || 4006;

app.listen(port, () => {
    console.log("server listening on port " + port);
})